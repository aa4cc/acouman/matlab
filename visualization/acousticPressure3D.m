function P = acousticPressure3D(phases,xLim,xElements,yLim,yElements,zLim,zElements)
% ACOUSTICPRESSURE3D Calculates the 3D acoustic pressure field
%  P = acousticPressure3D(phases,xLim,xElements,yLim,yElements,zLim,zElements,tx,ty)
%  Returns an yElements-by-xElements-by-zElements 3D acoustic pressure
%  field with the given phase delays. The transducers are modeled
%  as vibrating pistons.

%%% transducer radius
r = 0.005; 
global directivity sinAngle

num_trans = numel(phases);
array_size = sqrt(num_trans);
transducer_coordinates = linspace(-r*double(array_size-1),r*double(array_size-1),array_size)';
tx = reshape(kron(transducer_coordinates, ones(array_size,1)), 1, 1, 1, num_trans);
ty = reshape(repmat(transducer_coordinates, array_size, 1), 1, 1, 1, num_trans);


%%% transducer constant 
P0=5; 
%%% emitted frequency
f = 40000; 
w = 2*pi*f; 
%%% sound velocity in the carrier fluid (air) 
c0 = 343; 
k = w/c0;



x = linspace(xLim(1),xLim(2),xElements);
y = linspace(yLim(1),yLim(2),yElements);
z = linspace(zLim(1),zLim(2),zElements);
[x,y,z] = meshgrid(x,y,z);

X = repmat(x,1,1,1,num_trans) - repmat(tx,yElements,xElements,zElements,1);
Y = repmat(y,1,1,1,num_trans) - repmat(ty,yElements,xElements,zElements,1);
Z = repmat(z,1,1,1,num_trans);
phi = repmat(reshape(phases,1,1,1,num_trans),yElements,xElements,zElements,1);

    d = sqrt(X.^2 + Y.^2 + Z.^2);
    sinTheta = sqrt(X.^2 + Y.^2)./d;
    dirFunc = 2*besselj(1, k.*r.*sinTheta)./(k.*r.*sinTheta);
    dirFunc(isnan(dirFunc)) = 1;
%     dirFunc = interp1(sinAngle, directivity, sinTheta);
    p = exp(1i*(d.*k+phi)).*P0.*dirFunc./d;
    P = abs(sum(p,4));
end

